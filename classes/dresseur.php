<?php
class Dresseur
{
    //propriété de la carte dresseur
    private $name;
    private $deck;
    private $inventaire = [];
    // cration de la carte dresseur a partir de la classe
    public function __construct($name, $deck, $inventaire)
    {
        $this->name = $name;
        $this->deck = $deck;
        $this->inventaire = $inventaire;
    }
    public function voirInventaire()
    {
        if (empty($this->inventaire)) {
            echo ("\nVous perdez votre temps a chercher dans votre sac...vide....\n");
            return false;
        } else {
            foreach ($this->inventaire as $item) {
                echo ("\n$item->name\n");
            }
            return true;
        }
    }
    public function Utilise($objet, $cible)
    {
        $utilise = false;
        for ($i = 0; $i < $this->inventaire; $i++) {
            if ($this->inventaire[$i]->name == $objet) {
                $cible->soin($this->inventaire[$i], $cible);
                unset($this->inventaire[$i]);
                echo ("\nremove\n");
                $utilise = true;
                break;
            }
        }


    }
}
?>