<?php
//Classe pour un pokemon
class Pokemon
{
//propriété du pokemon
    private string $name;
    private string $type;
    private int $pv;
    private int $pvActuel;
    private int $niveau;
    private string $evolutionPrecedente;
    private string $extension;
    private int $numCarte;
    private $attaque;
    private $defense;
    private $enVie = true;
// cration du pokemon a partir de la classe
    public function __construct(string $name, string $type, int $pv,$pvActuel, int $niveau, string $evolutionPrecedente, string $extension, int $numCarte,$attaque,$defense)
    {
        $this->name = $name;
        $this->type = $type;
        $this->pv = $pv;
        $this->pvActuel = $pv;
        $this->niveau = $niveau;
        $this->evolutionPrecedente = $evolutionPrecedente;
        $this->extension = $extension;
        $this->numCarte = $numCarte;
        $this->attaque = $attaque;
        $this->defense = $defense;
        
    }

//permet de faire combatre 2 pokemon (la cible est la pokemon victime de l'attaque)
    public function Attaquer($cible ){
        $degat = $this->attaque;
        $degat -= $cible->defense;
        $cible->EtreAttaque($degat);
    }


//permet de faire perdre de la vie a un pokemon
    public function EtreAttaque($degat){
        $this->pvActuel -= $degat;
        if ($this->Mort()==true){
            echo("L'attaque a réussi\n");

        } else {
            echo("$this->name a sucombé a ses blesures\n");
        }
    }

//permet d'afficher une propriété d'un pokemon avec le parametre show (show est une string)
    public function Afficher($show){
        switch($show) {
            case "pv" :
                echo ("$this->pvActuel pv\n");
            break;
            case "name" :
                echo ("$this->name\n");
            break;
        }    
    }

//Permet de savoir si le pokemon est encore en vie ou non
    public function Mort(){
        if($this->pvActuel <= 0){
            $this->pvActuel = 0;
            $this->enVie = false;
        }
        return $this->enVie;
    }
    public function soin($objet,$cible) {
        $cible->pvActuel += $objet->nombre;
        if($cible->pvActuel > $cible->pv){
            $cible->pvActuel = $cible->pv;
        }
    }
    public function deBug(){
        var_dump($this);
    }
}
?>