<?php
echo ("\n");
include("classes/objet.php");
include("classes/soin.php");
include("classes/pokemon.php");
include("classes/dresseur.php");
//liste des objets
$potion = new Soin(150, "potion", "Soigne le pokemon de 150");

//créer un personnage
$personnage = new Dresseur("hero", "", [$potion, $potion]);

// Permet de choisir son pokemon parmi les 3 suivants
echo ("\nMerci de ne pas utilisez de majuscule lors de vos entrez, cela va casser le programme(ok != OK)\n");
sleep(1.5);
$Salameche = new Pokemon("Salameche", "feu", 150, 150, 3, "", "1ER", 1, 50000, 15);
$Bulbizar = new Pokemon("Bulbizar", "plante", 170, 170, 3, "", "1ER", 2, 45, 20);
$Carapuce = new Pokemon("Carapuce", "eau", 200, 200, 3, "", "1ER", 3, 40, 25);
echo ("Choisi ton pokemon :" . "\n" . "Salameche \nBulbizar \nCarapuce \n");
$choix = readline();
switch ($choix) {
    case "salameche":
        $pokemon1 = $Salameche;
        $pokemon2 = $Carapuce;
        break;
    case "bulbizar":
        $pokemon1 = $Bulbizar;
        $pokemon2 = $Salameche;
        break;
    case "carapuce":
        $pokemon1 = $Carapuce;
        $pokemon2 = $Bulbizar;
        break;
    default:
        echo ("Erreur");
}
echo ("Vous avez choisi ");
$pokemon1->Afficher("name");
sleep(1.5);
echo ("\nVotre rival a choisi ");
$pokemon2->Afficher("name");
sleep(1.5);

//Debut du combat

echo ("\nCOMBAT \n");
sleep(1.5);
echo ("Vous commencez votre 1ER combat, utiliser les differentes commande afin de faire une action \nattaque pour attaquer\n\ninventaire pour voir votre inventaire et utiliser un objet\n");
sleep(1.5);
$tour = 1;
while ($pokemon1->Mort() && $pokemon2->Mort()) {
    echo ("Tour n°$tour\n");
    echo ("A toi de jouer\n");
    $choix = readline();
    switch ($choix) {
        case "attaque":
            $pokemon1->Attaquer($pokemon2);
            $pokemon2->Afficher("pv");
            break;
        case "inventaire":
            if ($personnage->voirInventaire()) {
                echo ("\nChoissez l'item\n");
                $choix = readline();
                $personnage->Utilise($choix, $pokemon1);
                $pokemon1->Afficher("pv");
                echo ("\nVotre pokemon a été soigné\n");
            }
    }
    if ($pokemon2->Mort()) {
        echo ("Le tour de l'adversaire\n");
        sleep(1.5);
        $pokemon2->Attaquer($pokemon1);
        $pokemon1->Afficher("pv");
        $tour += 1;
    }
    sleep(1.5);
}
if ($pokemon1->Mort()) {
    echo ("vous avez gagné");
}
if ($pokemon2->Mort()) {
    echo ("vous avez perdu");
}













echo ("\n");
//affiche un deck
// $plateauJ1->randomDeck();
// var_dump($plateauJ1->deck);
?>